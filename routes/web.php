<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Doctor\DoctorController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::view('/login', 'dashboard.user.login')->name('login');
Route::view('/register', 'dashboard.user.register')->name('register');

Route::prefix('user')->name('user.')->group(function () {

    // Route::middleware('auth')->group(function () {
    //   Route::view('/login','dashboard.user.login')->name('login');
    //   Route::view('/register','dashboard.user.register')->name('register');
    Route::post('/create', [UserController::class, 'create'])->name('create');
    Route::post('/check', [UserController::class, 'check'])->name('check');
    // });

    Route::middleware(['auth:web', 'PreventBackHistory'])->group(function () {
        Route::view('/home', 'dashboard.user.home')->name('home');
        Route::post('/logout', [UserController::class, 'logout'])->name('logout');
        Route::get('/add-new', [UserController::class, 'add'])->name('add');
    });
});

Route::prefix('admin')->name('admin.')->group(function () {

    Route::middleware(['auth:admin', 'PreventBackHistory'])->group(function () {
        Route::view('/home', 'dashboard.admin.home')->name('home');
        Route::get('/create-affiliate', [AdminController::class, 'create'])->name('create.affiliate');
        Route::post('/store-affiliate', [AdminController::class, 'store'])->name('store.affiliate');
        // Route::post('/logout', [AdminController::class, 'logout'])->name('logout');
    });
});

Route::prefix('doctor')->name('doctor.')->group(function () {

    Route::middleware(['auth:doctor', 'PreventBackHistory'])->group(function () {
        Route::view('/home', 'dashboard.doctor.home')->name('home');
        Route::post('logout', [DoctorController::class, 'logout'])->name('logout');
    });
});
Route::prefix('affiliate')->name('affiliate.')->group(function () {

    Route::middleware(['auth:affiliate', 'PreventBackHistory'])->group(function () {
        Route::view('/home', 'dashboard.affiliate.home')->name('home');
        Route::post('logout', [DoctorController::class, 'logout'])->name('logout');
    });
});
Route::prefix('sub_affiliate')->name('sub_affiliate.')->group(function () {

    Route::middleware(['auth:sub_affiliate', 'PreventBackHistory'])->group(function () {
        Route::view('/home', 'dashboard.sub_affiliate.home')->name('home');
        Route::post('logout', [DoctorController::class, 'logout'])->name('logout');
    });
});
