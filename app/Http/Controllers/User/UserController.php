<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    function create(Request $request)
    {
        //Validate Inputs
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5|max:30',
            'cpassword' => 'required|min:5|max:30|same:password'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = \Hash::make($request->password);
        $save = $user->save();

        if ($save) {
            return redirect()->back()->with('success', 'You are now registered successfully');
        } else {
            return redirect()->back()->with('fail', 'Something went wrong, failed to register');
        }
    }

    function check(Request $request)
    {
        //Validate inputs
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:5|max:30'
        ]);
        $creds = $request->only('email', 'password');
        if (Auth::guard('web')->attempt($creds)) {
            return redirect()->route('user.home');
        } elseif (Auth::guard('admin')->attempt($creds)) {
            return redirect()->route('admin.home');
        } elseif (Auth::guard('doctor')->attempt($creds)) {
            return redirect()->route('doctor.home');
        } elseif (Auth::guard('affiliate')->attempt($creds)) {
            return redirect()->route('affiliate.home');
        } elseif (Auth::guard('sub_affiliate')->attempt($creds)) {
            return redirect()->route('sub_affiliate.home');
        }
        return redirect()->route('login')->with('fail', 'Incorrect credentials');
    }

    function logout()
    {
        auth()->logout();
        return redirect('/');
    }
}
