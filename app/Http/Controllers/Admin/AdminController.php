<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin;
use App\Models\Affiliate;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{


    public  function create()
    {
        return view('dashboard.admin.affiliate.create');
    }
    public  function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:5|max:30',
            'cpassword' => 'required|min:5|max:30|same:password'
        ]);

        $affiliate =  Affiliate::create([
            'name' => $request->name,
            'email' => $request->email,
            'admin_id' =>  Auth::guard('admin')->user()->admin_id,
            'promo_code' => mt_rand(1000, 9999),
            'password' => \Hash::make($request->password)
        ]);

        if ($affiliate) {
            return redirect()->back()->with('success', 'Affiliate created successfully');
        } else {
            return redirect()->back()->with('fail', 'Something went wrong, failed to create');
        }
    }
}
