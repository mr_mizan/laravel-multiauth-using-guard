<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Dashboard | Home</title>
    <link rel="stylesheet" href="{{ asset('bootstrap.min.css') }}">
</head>
<body style="background-color: #d7dadb">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3" style="margin-top: 45px">
                 <h4>Admin Dashboard || {{ Auth::guard('admin')->user()->name }} || {{ Auth::guard('admin')->user()->email }} || <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                   
                    <h3>Create Affiliate --<a href="{{ route('admin.home') }}">list</a> </h3>
                   
                        
                              <form action="{{ route('admin.store.affiliate') }}" method="post" autocomplete="off">
                                @csrf
                                @if (Session::get('success'))
                                     <div class="alert alert-success">
                                         {{ Session::get('success') }}
                                     </div>
                                @endif
                                @if (Session::get('fail'))
                                <div class="alert alert-danger">
                                    {{ Session::get('fail') }}
                                </div>
                                @endif
            
                                  <div class="form-group">
                                      <label for="name">Name</label>
                                      <input type="text" class="form-control" name="name" placeholder="Enter full name" value="">
                                      <span class="text-danger">@error('name'){{ $message }} @enderror</span>
                                  </div>
                                  <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Enter email address" value="">
                                    <span class="text-danger">@error('email'){{ $message }} @enderror</span>
                                </div>
                                  <div class="form-group">
                                      <label for="password">Password</label>
                                      <input type="password" class="form-control" name="password" placeholder="Enter password" value="">
                                      <span class="text-danger">@error('password'){{ $message }} @enderror</span>
                                  </div>
                                  <div class="form-group">
                                    <label for="cpassword">Confirm Password</label>
                                    <input type="password" class="form-control" name="cpassword" placeholder="Enter confirm password" value="">
                                    <span class="text-danger">@error('cpassword'){{ $message }} @enderror</span>
                                </div>
                                  <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary" value="Submit">
                                    <input type="reset" class="form-control btn btn-info" value="Reset">
                                </div>
                               
                              </form>
                        </div>
            </div>
        </div>
    </div>
</body>
</html>